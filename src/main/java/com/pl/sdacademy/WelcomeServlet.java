package pl.sdacademy;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * http://dominisz.pl
 * 23.09.2017
 */
@WebServlet(value = "/welcome",
        initParams = {@WebInitParam(name = "hasFooter", value = "true"),
                @WebInitParam(name = "companyName", value = "Software Development Academy"),
                @WebInitParam(name = "year", value = "2015")})
public class WelcomeServlet extends HttpServlet {


    private boolean hasFooter;
    private String companyName;
    private int year;

    @Override
    public void init(ServletConfig config) throws ServletException {
        hasFooter = Boolean.valueOf(config.getInitParameter("hasFooter"));
        companyName = config.getInitParameter("companyName");
        if (companyName == null) {
            companyName = "Company Name";
        }
        String initParameter = config.getInitParameter("year");
        if (initParameter == null) {
            initParameter = "2017";
        }
        try {
            year = Integer.parseInt(initParameter);
        } catch (NumberFormatException ex) {
            year = 2017;
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp, "HTTP GET METHOD");
    }

    private void processRequest(HttpServletRequest req, HttpServletResponse resp, String message) throws IOException {
        resp.setContentType("text/html; charset=UTF-8");
        resp.setCharacterEncoding("UTF-8");
        PrintWriter out = resp.getWriter();
        out.println("Zażółć gęsią jaźń");
        out.println("<h1>Welcome!</h1>");
        out.println(message);

        if (hasFooter) {
            printFooter(out);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp, "HTTP POST METHOD");
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp, "HTTP PUT METHOD");
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp, "HTTP DELETE METHOD");
    }

    private void printFooter(PrintWriter out) {
        out.println("<hr>");
        out.println(companyName + " " + year);
    }

}